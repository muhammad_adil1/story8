from django.test import TestCase, override_settings, Client, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options

class UnitTest(TestCase):
    def test_views_success(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(
            chrome_options=chrome_options, executable_path='./chromedriver')
    def tearDown(self):
        self.driver.quit()
        super().tearDown()
    def test_page(self):
        #Adil open his Firefox and the website
        self.driver.get(self.live_server_url)
        
        #Adil see the items in the page
        time.sleep(2)
        image = self.driver.find_element_by_id('myImage')
        name = self.driver.find_element_by_name('nama')
        accordion = self.driver.find_elements_by_class_name('accordion')
        accordion_button_down = self.driver.find_elements_by_class_name('buttondown')
        accordion_button_up = self.driver.find_elements_by_class_name('buttonup')
        #Adil try clicking the accordion
        for i in range(4):
            accordion[i].click()
            time.sleep(1)
        #Adil look at the content
        self.assertIn('Muhammad', self.driver.page_source)
        self.assertIn('PPW', self.driver.page_source)
        self.assertIn('Bekasi', self.driver.page_source)
        self.assertIn('tujuan', self.driver.page_source)
        
        #Adil try clicking the button down
        for i in range(4):
            accordion_button_down[i].click()
            time.sleep(1)
        #Adil see the position change
        accordion_button_down_now = self.driver.find_elements_by_class_name('buttondown')
        self.assertIsNot(accordion_button_down, accordion_button_down_now)
        #Adil try clicking the button up
        for i in range(4):
            accordion_button_up[i].click()
            time.sleep(1)
        accordion_button_up_now = self.driver.find_elements_by_class_name('buttonup')
        self.assertIsNot(accordion_button_up, accordion_button_up_now)